FROM golang

RUN git clone https://github.com/assetnote/kiterunner.git && \
    cd kiterunner && \
    make build && \
    mv dist/kr /usr/local/bin/kr && \
    cd .. && \
    rm -rf kiterunner && \
    wget -qO- https://wordlists-cdn.assetnote.io/data/kiterunner/routes-large.kite.tar.gz | tar xvz -C .
